$('.services-tabs').on('click', function () {
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
    const dataValue = $(this).data('tab');
    $('.services-details').removeClass('active');
    $(`.services-details[data-tab=${dataValue}]`).addClass('active');
})

$('.amazing-work-navigation').on('click', function (e) {
    e.preventDefault();
    const dataValue = $(e.target).data('category');
    if (dataValue === "cat-all") {
        $('.amazing-work-info-block').addClass('sorted');
        $(e.target).siblings().removeClass('active')
        $(e.target).addClass('active')
    } else {
        $('.amazing-work-info-block').removeClass('sorted');
        $(`.amazing-work-info-block.${dataValue}`).addClass('sorted');
        $(e.target).siblings().removeClass('active')
        $(e.target).addClass('active')
    }
});

$('.amazing-work-button-load').on('click', function (e) {
    e.preventDefault();
    const arr = $('.amazing-work-info-block');
    const loader = $('.tea');

    if (arr.length === 12) {
        $(e.target).css('opacity', '0');
        loader.css('display', 'block')
        
        const timerHide = setTimeout(function () {
            $('.amazing-work-info-block-hiden-1').removeClass('amazing-work-info-block-hiden-1').addClass('amazing-work-info-block')
            $(e.target).css('opacity', '1');
        loader.css('display', 'none')
        }, 2000);


    } else if (arr.length === 24) {
        $(e.target).css('opacity', '0');
        loader.css('display', 'block')
        const timerHide = setTimeout(function(){
            $('.amazing-work-info-block-hiden-2').removeClass('amazing-work-info-block-hiden-2').addClass('amazing-work-info-block');
            $(e.target).remove();
            $('.amazing-work').css('padding', '0 0 50px');
            loader.css('display', 'none');
        },2000) 
    }
})

$('.carousel-list').on('click', function (e) {
    e.preventDefault();
    if ($(e.target).hasClass('arrow-right')) {
        const activeUser = $('.about-carousel.icons-user.active');
        const nextActiveUser = activeUser.next('.about-carousel.icons-user');
        if (nextActiveUser.length) {
            activeUser.fadeOut('fast').removeClass('active');
            nextActiveUser.fadeIn(400).addClass('active')
            const dataValue = $(nextActiveUser).data('user')
            $('.carousel-unit.active').removeClass('active');
            $(`.carousel-unit[data-user=${dataValue}]`).addClass('active');
        }
    } else if ($(e.target).hasClass('arrow-left')) {
        const activeUser = $('.about-carousel.icons-user.active')
        const prevActiveUser = activeUser.prev('.about-carousel.icons-user');
        
        if (prevActiveUser.length) {
            activeUser.fadeOut('fast').removeClass('active')
            prevActiveUser.fadeIn(400).addClass('active');
            const dataValue = $(prevActiveUser).data('user')
            $('.carousel-unit.active').removeClass('active');
            $(`.carousel-unit[data-user=${dataValue}]`).addClass('active');
        }
    }
    else if ($(e.target).hasClass('carousel-unit')) {
        $('.carousel-unit.active').removeClass('active');
        const activeUser = $(e.target).addClass('active');
        $('.about-carousel.icons-user.active').fadeOut('fast').removeClass('active');
        const dataValue = $(activeUser).data('user')
        $(`.about-carousel.icons-user[data-user=${dataValue}]`).fadeIn(400).addClass('active');
    }
});