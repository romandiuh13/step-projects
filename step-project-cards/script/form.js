import {Component} from './component.js'

class Form extends Component{
    render() {
        const {text, activeClass, ...attr} = this.props;
        const btn = this.createElement("button", attr, text)
        // btn.addEventListener('click', this.handleClick);
        this.elem = btn;
        return btn
    }
}

const newButton = {
    text: "Нажми меня",
    id: "click-btn",
    className: "btn btn-primary",
    activeClass: "active"
};

const button = new Form(newButton);
document.body.append(button.render());